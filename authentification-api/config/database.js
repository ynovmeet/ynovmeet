const Sequelize = require("sequelize");

const sequelize = new Sequelize("mysqlNode", "root", "", {
  hostname: "localhost",
  dialect: "mysql",
  port: '3307',
});

sequelize

  .authenticate()
  .then(() => {
    console.log(" connection has been esblished successfuly");
  })
  .catch(err => {
    console.log(" unable to connect");
  });

module.exports = sequelize;
